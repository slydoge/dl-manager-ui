// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

var {ipcRenderer, remote} = require('electron');

$("#start-dl").click(function(){
	ipcRenderer.send("start-dl", $("#url").val());
});
$("#stop-dl").click(function(){
	ipcRenderer.send("stop-dl");
});

ipcRenderer.on('progress', (event, arg) => {
	console.log(arg);
	$("#set-progress").css("width", arg + "%");
	$("#set-progress").text(arg + "%");
});

ipcRenderer.on('done', (event) => {
	$("#set-progress").removeClass("active");
	$("#set-progress").removeClass("progress-bar-striped");
	$("#set-progress").addClass("progress-bar-success");
});